# Zadanie rekrutacyjne

Zadanie rekrutacyjne do firmy Smart Factor, w którym wykonam następujące zadania:
1. Przygotouje endpointy do wczytywania, zapisywania i modyfikacji bazy danych;
2. Baza danych będzie filtrowana po atrybutach oraz wyświetlana w kolejności alfabetycznej;
3. Do metody filtrującej zostaną dodane testy;